FROM camunda/zeebe:8.2.5

HEALTHCHECK --interval=30s --timeout=5s --retries=3 CMD curl -f http://localhost:9600/ready || exit 1